SHELL=/bin/sh

.PHONY: all

all: latex

task: pdflatex-task lint-task

pdflatex-task:
	pdflatex task.tex

lint-task:
	git add -u task.tex task_title.tex
	which z-djail && \
		test -d latexindent.pl-master && \
		z-djail \
			perl latexindent.pl-master/latexindent.pl \
				--overwrite \
			  	--modifylinebreaks \
					task.tex task_title.tex

show-task: task
	xdg-open task.pdf

#test:
#	PYTHONPATH=. ./venv/bin/pytest -v tests/test*.py

jupyter-lab:
	./venv/bin/jupyter lab

#measure-task:
#	z-cpufreq-800
#	./venv/bin/python task.py "CSV"
#	z-cpufreq-4300

#measure-task-quick:
#	./venv/bin/python task.py "CSV"

#measure-task-test:
#	env TEST_MODE=true ./venv/bin/python task.py "CSV"

#plot-task:
#	./venv/bin/python task.py "PLOT"

run:
	./venv/bin/python task.py
