import abc
import logging
import random
import sys
from dataclasses import dataclass

import matplotlib.pyplot as plt
import numpy as np
from matplotlib.axes import Axes
from matplotlib.figure import Figure
from scipy import optimize

WORKERS = 1
X = 0
Y = 1


@dataclass
class DataWrapper:
    seed: int = 2020  # default seed value for random class
    data = None

    def __post_init__(self):
        self.random = random.Random(self.seed)
        self.alpha = self.random.random()
        self.beta = self.random.random()

    def prepare_data(self) -> 'DataWrapper':
        """
        Prepares the data for the algorithms
        """
        assert self.data is None
        data = np.zeros((101, 2))
        for k in range(0, 100 + 1):
            delta = self.random.normalvariate(mu=0, sigma=1)
            x = float(k) / 100.0
            y = self.alpha * x + self.beta + delta

            data[k][X] = x
            data[k][Y] = y

        self.data = data
        return self


class Counter:
    def __init__(self):
        self._counter = 0

    def incr(self):
        self._counter += 1

    @property
    def counter(self):
        return self._counter


class Task(abc.ABC):
    def __init__(self, data_wrapper: DataWrapper):
        self.eval_count = 0
        self.data_wrapper: DataWrapper = data_wrapper

    @abc.abstractmethod
    def approximant_function(self, x, a, b):
        raise NotImplementedError()

    def means_of_least_squares(self, ab):
        """
        Function to optimize. Calculates means of least squares
        """
        a, b = ab
        assert self.data_wrapper is not None
        result = 0.0
        for k in range(0, 100 + 1):
            x = self.data_wrapper.data[k][0]
            y = self.data_wrapper.data[k][1]
            evaluated_value = self.approximant_function(x, a, b)
            result += (evaluated_value - y) ** 2

        logging.debug("a, b -> %s = %s", ab, result)
        self.eval_count += 1
        return result

    def plot(self, gd, cgd, newton, lm, output_plot_path):
        x = self.data_wrapper.data[:, 0]
        y_noisy = self.data_wrapper.data[:, 1]

        fig, ax = plt.subplots()
        fig: Figure
        ax: Axes

        y_clean = [
            self.data_wrapper.alpha * i + self.data_wrapper.beta
            for i in x
        ]

        y_gd = [
            self.approximant_function(i, gd['a'], gd['b'])
            for i in x
        ]

        y_cgd = [
            self.approximant_function(i, cgd['a'], cgd['b'])
            for i in x
        ]

        y_newton = [
            self.approximant_function(i, newton['a'], newton['b'])
            for i in x
        ]

        y_lm = [
            self.approximant_function(i, lm['a'], lm['b'])
            for i in x
        ]

        ax.plot(x, y_noisy, '.', alpha=0.5, label="$y = \\alpha x + \\beta + \\delta$")
        ax.plot(x, y_clean, '-', alpha=0.2, label="$y = \\alpha x + \\beta$")
        ax.plot(x, y_gd, '-', alpha=0.5, label="GD")
        ax.plot(x, y_cgd, '-', alpha=0.5, label="CGD")
        ax.plot(x, y_newton, '-', alpha=0.5, label="Newton's method")
        ax.plot(x, y_lm, '-', alpha=0.5, label="Levenberg Marquardt")

        ax.set_xlabel('x')
        ax.set_ylabel('y')
        ax.legend()

        fig.savefig(str(output_plot_path))

    # --------------------------------------------------
    # Gradient Descent
    # --------------------------------------------------

    def _gradient_descent_naive(self, x0, epsilon: float, iter_count: Counter):
        max_iter = 1000000
        c_values = [epsilon * 100,
                    epsilon * 10,
                    epsilon,
                    epsilon / 10.0,
                    epsilon / 100.0,
                    epsilon / 1000.0]
        a, b = x0
        count_fprimes_small = 0
        f_values = []
        c = c_values.pop(0)
        while True:
            iter_count.incr()
            if iter_count.counter > max_iter:
                raise Exception(f"No solution found after {max_iter} iterations")

            fprimes = optimize.approx_fprime(
                [a, b], self.means_of_least_squares, epsilon)
            if all([fp < 0.05 for fp in fprimes]):
                count_fprimes_small += 1
                if count_fprimes_small >= 10:
                    break
            else:
                count_fprimes_small = 0

            if iter_count.counter % 1000 == 0:
                logging.debug("%s iteraciones - ab: %s, fprimes: %s",
                              iter_count.counter, [a, b], fprimes)

            fp_a = fprimes[0]
            fp_b = fprimes[1]

            if abs(fp_a) > abs(fp_b):
                if fp_a < 0:
                    a, b = a + c, b
                else:
                    a, b = a - c, b
            else:
                if fp_b < 0:
                    a, b = a, b + c
                else:
                    a, b = a, b - c

            f_values.append(self.means_of_least_squares([a, b]))
            logging.debug("f-value: %s", f_values[-1])
            while len(f_values) > 10:
                f_values.pop(0)

            if len(f_values) == 10:
                if len(set(f_values)) <= 2:
                    if c_values:
                        c = c_values.pop(0)
                        logging.info("Trying smaller value for c: "
                                     "%s - a: %s - b: %s", c, a, b)
                    else:
                        break

        assert iter_count.counter < 1000000
        return a, b

    def gradient_descent(self, epsilon):
        logging.info("---------- Starting gradient_descent() ----------")
        sys.stdout.flush()

        self.eval_count = 0
        iter_count = Counter()
        initial_guess = [1, 1]
        res = self._gradient_descent_naive(
            initial_guess,
            epsilon=epsilon,
            iter_count=iter_count,
        )
        sys.stdout.flush()
        logging.info("Finished gradient_descent(): %s", res)
        sys.stdout.flush()
        return dict(a=res[0],
                    b=res[1],
                    eval_count=self.eval_count,
                    iter_count=iter_count.counter)

    # --------------------------------------------------
    # Conjugate Gradient Descent
    # --------------------------------------------------

    def conjugate_gradient_descent(self, epsilon):
        logging.info("---------- Starting conjugate_gradient_descent() ----------")
        sys.stdout.flush()

        self.eval_count = 0
        iter_count = Counter()
        initial_guess = [1, 1]
        xopt = optimize.fmin_cg(
            self.means_of_least_squares,
            initial_guess,
            epsilon=epsilon,
            full_output=True,
            callback=lambda x: iter_count.incr(),
        )
        sys.stdout.flush()
        logging.info("Finished conjugate_gradient_descent()")
        sys.stdout.flush()

        return dict(a=xopt[0][0],
                    b=xopt[0][1],
                    eval_count=self.eval_count,
                    iter_count=iter_count.counter)

    # --------------------------------------------------
    # Newton's method
    # --------------------------------------------------

    def newtons_method(self, epsilon, fail_on_error=True, initial_guess=None):
        logging.info("---------- Starting newtons_method() ----------")
        sys.stdout.flush()

        def compute_jacobian(ab):
            return optimize.approx_fprime(ab, self.means_of_least_squares, epsilon)

        self.eval_count = 0
        initial_guess = initial_guess or [1, 1]
        res: optimize.OptimizeResult = optimize.minimize(
            self.means_of_least_squares,
            initial_guess,
            options=dict(
                xtol=epsilon,
            ),
            method="Newton-CG",
            jac=compute_jacobian,
        )
        sys.stdout.flush()
        logging.info("Finished newtons_method()")
        sys.stdout.flush()

        if res.success:
            return dict(a=res.x[0],
                        b=res.x[1],
                        eval_count=self.eval_count,
                        iter_count=res.nit)
        else:
            if fail_on_error:
                raise Exception("newtons_method() was not successfull")
            else:
                logging.error("newtons_method() was not successfull")
                return dict(a=-999999,
                            b=-999999,
                            eval_count=self.eval_count,
                            iter_count=res.nit)

    # --------------------------------------------------
    # Levenberg-Marquardt algorithm
    # --------------------------------------------------

    def _fun_residual(self, ab):
        a, b = ab
        assert self.data_wrapper is not None
        residuals = list()
        for k in range(0, 100 + 1):
            x = self.data_wrapper.data[k][0]
            y = self.data_wrapper.data[k][1]
            evaluated_value = self.approximant_function(x, a, b)
            residual = evaluated_value - y
            residuals.append(residual)
        return residuals

    def levenberg_marquardt_algorithm(self, epsilon, initial_guess=None):
        logging.info("---------- Starting levenberg_marquardt_algorithm() ----------")
        sys.stdout.flush()
        initial_guess = initial_guess or [1, 1]
        res: optimize.OptimizeResult = optimize.least_squares(
            self._fun_residual,
            initial_guess,
            method='lm',
            xtol=epsilon,
            verbose=2,
        )
        sys.stdout.flush()
        logging.info("Finished levenberg_marquardt_algorithm(): res=%s", res)
        sys.stdout.flush()
        assert res.success, f"levenberg_marquardt_algorithm() was not successfull"
        return dict(a=res.x[0],
                    b=res.x[1],
                    eval_count=self.eval_count,
                    iter_count=-1)


class TaskLinear(Task):
    def approximant_function(self, x, a, b):
        return a * x + b


class TaskRational(Task):
    def approximant_function(self, x, a, b):
        return a / 1 + b * x


def main(epsilon=None, seed=None):
    logging.info("seed=%s", seed)
    data_wrapper = DataWrapper(seed=seed).prepare_data()
    linear = TaskLinear(data_wrapper)
    rational = TaskRational(data_wrapper)

    logging.info("alpha=%s", data_wrapper.alpha)
    logging.info("beta=%s", data_wrapper.beta)

    EPSILON = epsilon or 0.001

    # ============================================================
    # gradient_descent
    # ============================================================

    result_gd_linear = linear.gradient_descent(epsilon=EPSILON)
    result_gd_rational = rational.gradient_descent(epsilon=EPSILON)

    # ============================================================
    # conjugate_gradient_descent
    # ============================================================

    # Linear
    result_cgd_linear = linear.conjugate_gradient_descent(EPSILON)
    result_cgd_rational = rational.conjugate_gradient_descent(EPSILON)

    # ============================================================
    # newton
    # ============================================================

    result_newton_linear = linear.newtons_method(EPSILON)
    result_newton_rational = rational.newtons_method(EPSILON)

    # ============================================================
    # levenberg_marquardt_algorithm
    # ============================================================

    result_lm_linear = linear.levenberg_marquardt_algorithm(EPSILON)
    result_lm_rational = rational.levenberg_marquardt_algorithm(EPSILON)

    # ============================================================
    # Report
    # ============================================================

    logging.info("---------- REPORT ----------")
    sys.stdout.flush()

    def report(result):
        return ', '.join([f"{k}={result[k]}"
                          for k in sorted(result.keys())])

    logging.info("result_gd_linear: %s", report(result_gd_linear))
    logging.info("result_gd_rational: %s", report(result_gd_rational))

    logging.info("result_cgd_linear: %s", report(result_cgd_linear))
    logging.info("result_cgd_rational: %s", report(result_cgd_rational))

    logging.info("result_newton_linear: %s", report(result_newton_linear))
    logging.info("result_newton_rational: %s", report(result_newton_rational))

    logging.info("result_lm_linear: %s", report(result_lm_linear))
    logging.info("result_lm_rational: %s", report(result_lm_rational))

    linear.plot(
        result_gd_linear,
        result_cgd_linear,
        result_newton_linear,
        result_newton_linear,
        'data/linear.pdf'
    )
    rational.plot(
        result_gd_rational,
        result_cgd_rational,
        result_newton_rational,
        result_lm_rational,
        'data/rational.pdf'
    )

    # ============================================================
    # Report 2
    # ============================================================

    logging.info("---------- REPORT 2 ----------")
    sys.stdout.flush()

    logging.info("result_newton_linear: %s",
                 report(linear.newtons_method(EPSILON,
                                              initial_guess=[5.0, 5.0],
                                              fail_on_error=False)))
    logging.info("result_lm_linear: %s",
                 report(linear.levenberg_marquardt_algorithm(EPSILON,
                                                             initial_guess=[5.0, 5.0])))


def setup_logging(level=logging.DEBUG):
    logging.basicConfig(level=level, stream=sys.stdout)
    logging.getLogger('matplotlib').setLevel(logging.WARNING)


if __name__ == '__main__':
    setup_logging(level=logging.INFO)
    main(seed=6832299506)
    logging.info("Finished OK")
